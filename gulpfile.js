/**
 * Created by ahadrii on 27/01/17.
 */

const gulp = require('gulp');
const $ = require('gulp-load-plugins')({
    lazy: true,
});
const config = require('./gulp.config')();
const wiredep = require('wiredep').stream;


gulp.task('lint', () => {
    console.log('Analyzing source code with ESlint');
    return gulp.src(config.js)
.pipe($.eslint())
.pipe($.eslint.format());
// Brick on failure
// .pipe($.eslint.failOnError());
});


gulp.task('inject', () =>{
    console.log('Injecting css and js files');
    return gulp
    .src(config.index)
    .pipe($.inject(gulp.src(config.js_assets, {read: false})))
    .pipe($.inject(gulp.src(config.css, {read: false})))
    .pipe(gulp.dest('.'));
});


gulp.task('bower', ['inject'], ()=>{
  return gulp.src(config.index)
  .pipe(wiredep(config.getWiredepDefaultOptions()))
  .pipe(gulp.dest('.'));
});

gulp.task('webserver', () => {
	$.connect.server({
	livereload: true,
	});
});

gulp.task('watch', function() {
    gulp.watch(config.alljs).on('change', $.livereload.changed);
    gulp.watch(config.css_assets).on('change', $.livereload.changed);
    gulp.watch(config.index).on('change', $.livereload.changed);
    gulp.watch(config.views).on('change', $.livereload.changed);
  });

gulp.task('default', ['bower', 'lint', 'webserver'], () => {
    gulp.start('watch');
});
