/**
 * Created by ahadri on 1/27/17.
 */


const http = require('http');

const PORT = 9000;

const requestHandler = (req, res) => {
    console.log(req.url);
    res.end('Hello ICare Nodejs Server');
};
const server = http.createServer(requestHandler);

server.listen(PORT, (err) => {
    if (err) {
        return console.log('Something bad happened', err);
    }

    console.log(`Server is listening on ${PORT}`);
});
