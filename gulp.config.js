/**
 * Created by ahadrii on 27/01/17.
 */

module.exports = function() {
    const assets = './assets/';
    const partials = './partials/';

    let config = {
        alljs: [assets + 'js/*.js', './app.js', './controllers/**/*.js',
        './services/**/*.js', './factories/**/*.js',
        './config/**/*.js', './gulp.config.js', './gulpfile.js'],

        index: './index.html',
        css: [assets + 'css/*.css'],
        js_assets: [assets + 'js/*.js'],
        js: ['./controllers/**/*.js',
        './services/**/*.js', './factories/**/*.js',
        './config/**/*.js'],
        views: [partials + '*.html', partials + '**/*.html'],
        bower: {
            json: require('./bower.json'),
            directory: './bower_components/',
            ignorePath: '../..',
          },
      };

    // Wiredep settings
    config.getWiredepDefaultOptions = function() {
      let options = {
          bowerJson: config.bower.json,
          directory: config.bower.directory,
          ignorePath: config.bower.ignorePath,
        };
      return options;
    };


    return config;
  };

