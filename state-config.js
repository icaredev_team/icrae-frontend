/**
 * By FalseBytes on 27-01-2017
 */

(function() {
	angular
		.module('main')
		.config(config);


	config.$inject = ['$stateProvider', '$urlRouterProvider'];
	/**
	 *  Config the states of the app
	 * @param {$stateProvider}  $stateProvider
	 * @param {$urlRouterProvider}   $urlRouterProvider
	 */
	function config($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/dashboard');

		$stateProvider
			.state('index', {
				abstract: true,
				url: '',
				views: {
					'header@': {
						templateUrl: 'partials/header.html',
						controller: 'HeaderController',
					},
					'footer@': {
						templateUrl: 'partials/footer.html',
						controller: 'FooterController',
					},
				},
			})
			.state('index.home', {
				url: '/home',
				views: {
					'body@': {
						templateUrl: 'partials/index/body.html',
						controller: 'BodyController',
					},
				},
			})
			.state('index.addride', {
			url: '/addride',
				views: {
				'body@': {
					templateUrl: 'partials/add-ride/body.html',
					controller: 'AddRideController',
				},
				},
			})
			.state('index.ridedetails', {
				url: '/ridedetails',
				params: {ride: null},
				views: {
					'body@': {
						templateUrl: 'partials/ride-details/body.html',
						controller: 'RideDetailsController',
					},
				},
			})
			.state('index.searchrides', {
				url: '/searchrides',
				views: {
					'body@': {
						templateUrl: 'partials/search-rides/body.html',
						controller: 'RidesSearchController',
					},
				},
			})
			.state('index.driverdashboard', {
				url: '/dashboard',
				views: {
					'body@': {
						templateUrl: 'partials/driver-dashboard/driver.dashboard.html',
						controller: 'DriverDashboardController',
					},
				},
			});
	}
})();


