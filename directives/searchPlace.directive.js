angular.module('main')
	.directive('googlePlace', directiveFunction);

directiveFunction.$inject = ['$rootScope'];
/**
 *
 * @param {$rootScope} $rootScope
 * @return {{require: string, scope: {ngModel: string,
 * details: string}, link: link}}
 */
function directiveFunction($rootScope) {
	return {
		require: 'ngModel',
		scope: {
			ngModel: '=',
			details: '=?',
		},
		link: function(scope, element, attrs, model) {
			scope.gPlace = new google.maps.places.Autocomplete(element[0], {});
			google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
				scope.$apply(function() {
					model.$setViewValue(element.val());
				});
			});
		},
	};
}
