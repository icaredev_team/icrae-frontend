/**
 * Created by ahadri on 1/28/17.
 */

(function() {
	angular.module('main')
		.controller('SignUpController', SignUpController);
	/**
	 * The Constructor function of the SignUp Controller
	 * @constructor
	 */
	function SignUpController() {
		this.email = '';
		this.username = '';
		this.password1 = '';
		this.password2 = '';
		this.signUp = signUp;

		/**
		 * The signUp function : Do the logic for signup the new user
		 */
		function signUp() {
			if (this.password1 === this.password2) {
				console.log(this.password1 + ' and ' + this.password2 + ' match');
			} else {
				console.log(this.password1 + ' and ' + this.password2 + ' don\'t match');
			}
		}
	}
})();

