/**
 * Created by ahadri on 1/27/17.
 */

/**
 * Created by ahadri on 1/27/17.
 */
(function() {
	'use strict';
	angular.module('main')
		.controller('BodyController', BodyController);
	/**
	 * The Constructor function of the body partial controller
	 * @constructor
	 */
	function BodyController() {
		let vm = this;

		vm.fromPlace = '';
		vm.toPlace = '';
		vm.findRide = findRide;
		vm.rideDate = new Date();

		/**
		 * Launch the findRide Functionality
		 */
		function findRide() {
			console.log('from = ' + vm.fromPlace + ', to = ' + vm.toPlace, ', at  = ' + vm.rideDate.toDateString());
		}
	}
})();
