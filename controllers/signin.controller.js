/**
 * Created by ahadri on 1/28/17.
 */

(function() {
	'use strict';
	angular.module('main')
		.controller('SignInController', SignInController);
	/**
	 * The Constructor function of the SignIn Controller
	 * @constructor
	 */
	function SignInController() {
		this.email = '';
		this.password = '';
		this.signIn = signIn;

		/**
		 * The signIn function : Do the logic for signIn
		 */
		function signIn() {
			console.log('Email: ' + this.email + ', Password: ' + this.password);
		}
	}
})();
