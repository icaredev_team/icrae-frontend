/**
 * Created by ahadri on 2/1/17.
 */

(function() {
    'use strict';
    angular.module('main')
        .controller('DriverDashboardController', DriverDashboardController);
	DriverDashboardController.$inject = ['DriverDashboardService'];
	/**
	 *
	 * @constructor
	 */
	function DriverDashboardController(DriverDashboardService) {
        let vm = this;
        vm.dashboardService = DriverDashboardService;
		vm.requests = vm.dashboardService.rideRequests;
		vm.acceptRequests = acceptRequests;
		vm.user = {
			'firstname': 'Hatim',
			'lastname': 'Ahadri',
			'avis': 6,
			'email': 'ahadrihatim@gmail.com',
			'age': 20,
			'img': 'assets/img/hatim.png',
			'rating': 3,
			'preference': {'music': true, 'blabla': false},
		'car': {'mark': 'Clio', 'color': 'Gris Clair', 'matricule': '12-b-102'},
		};


		function acceptRequests() {
			vm.requests = vm.requests.filter((request) => request.response === false);
		}
    }
})();
