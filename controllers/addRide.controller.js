/**
 * Created by ahadri on 1/29/17.
 */

(function() {
	angular.module('main')
		.controller('AddRideController', AddRideController);
	/**
	 * The Constructor function of the Add Ride Controller
	 * @constructor
	 */
	function AddRideController() {
		let vm = this;
		vm.rideDate = new Date();
		vm.ridePrice = '';
		vm.numbers = [1, 2, 3, 4];
		vm.ridePlacesAvailable = 1;
		vm.moreInfo = '';
		vm.departure = '';
		vm.arrival = '';
		vm.createRide = createRide;


		/**
		 *
		 */
		function createRide() {
			console.log(vm.rideDate.toDateString() + ', ' + vm.ridePrice + ', ' + vm.ridePlacesAvailable + ', ' + vm.departure + ', ' + vm.arrival + ', ' + vm.moreInfo);
		}
	}
})();
