/**
 * Created by ahadri on 1/30/17.
 */

(function() {
    angular.module('main')
        .controller('RideDetailsController', RideDetailsController);
    RideDetailsController.$inject = ['$stateParams'];
	/**
	 *  $stateParams {$stateParams}
	 * @constructor
	 */
	function RideDetailsController($stateParams) {
        let vm = this;
		vm.getRidePrice = '';
		vm.getRidePlaces = '';
		vm.getDriverInfo = '';
		vm.getDriverRating = '';
		vm.getCarInfo = '';
		vm.ride = getRide();
		/**
		 *
		 * @return {ride|*}
		 */
		function getRide() {
		let ride = $stateParams.ride;
		if (ride != null) {
			vm.getRidePrice = ride.price + ' Dhs/Place';
			vm.getRidePlaces = ride.placesAvailable + ' places available';
			vm.getDriverInfo = ride.driver.firstname + ' ' + ride.driver.lastname + '\n' + ride.driver.age + ' years';
//			vm.getDriverRating = ride.driver.rating + '/5 - ' + ride.driver.avis + ' rating';
			vm.getCarInfo = ride.car.mark + ', Color : ' + ride.car.color;
			vm.getDriverRating = '0/5  rating';

			return ride;
		}
		}
    }
})();
