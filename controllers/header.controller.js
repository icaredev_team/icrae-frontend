
(function() {
    angular.module('main')
        .controller('HeaderController', HeaderController);
	HeaderController.$inject = ['$uibModal'];
	/**
	 * The constructor function of the header partial controller
	 * @param  {$uibModal}  $uibModal
	 * @constructor
	 */
	function HeaderController($uibModal) {
		this.signIn = signIn;
		this.signUp = signUp;


		/**
		 * Fire up the modal for the signIn
		 */
		function signIn() {
			this.signInModalInstance = $uibModal.open({
				animation: true,
				templateUrl: '../partials/authentication/signin.modal.html',
				controller: 'SignInController',
				controllerAs: 'controller',
			});
		}

		/**
		 * Fire up the modal for the signUp
		 */
		function signUp() {
			this.signInModalInstance = $uibModal.open({
				animation: true,
				templateUrl: '../partials/authentication/signup.modal.html',
				controller: 'SignUpController',
				controllerAs: 'controller',
			});
		}
    }
})();
