/**
 * Created by ahadri on 1/30/17.
 */


(function() {
    angular.module('main')
        .controller('RidesSearchController', RidesSearchController);

    RidesSearchController.$inject = ['SearchRidesService', '$state'];
	/**
	 *   {$state}  $state
	 * @constructor
	 */
	function RidesSearchController(SearchRidesService, $state) {
		let vm = this;
		vm.searchByPrice = 500;
		vm.searchByDriverName = '';
		vm.searchService = SearchRidesService;
		vm.rides = vm.searchService.searchRides;
		vm.filterByPrice = filterByPrice;
		vm.moreInfo = moreInfo;

		/**
		 *
		 * @param {ride} ride
		 * @return {boolean}
		 */
		function filterByPrice(ride) {
			return (ride.price <= vm.searchByPrice);
		}

		/**
		 *
		 * @param {ride} ride
		 */
		function moreInfo(ride) {
			let stateParams = {ride: ride};
			$state.go('index.ridedetails', stateParams);
		}
    }
})();
