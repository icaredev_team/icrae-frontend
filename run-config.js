/**
 * Created by ahadri on 2/4/17.
 */


(function() {
	'use strict';

	angular
		.module('main')
		.config(ConfigRun);

	ConfigRun.$inject = ['$mdIconProvider'];

	/**
	 *
	 * @param $mdIconProvider  {$mdIconProvider}
	 * @constructor
	 */
	function ConfigRun($mdIconProvider) {
		$mdIconProvider
			.iconSet('email', 'assets/img/email.svg', 24);
	}
});
