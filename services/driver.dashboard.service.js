/**
 * Created by ahadri on 2/1/17.
 */

(function() {
    angular.module('main')
        .service('DriverDashboardService', DriverDashboardService);
	/**
     *
	 * @constructor
	 */
	function DriverDashboardService() {
		let api = {
			'rideRequests': [
//				{
//					'firstname': 'Hatim',
//					'lastname': 'Ahadri',
//					'avis': 6,
//					'email': 'ahadrihatim@gmail.com',
//					'age': 20,
//					'img': 'assets/img/hatim.png',
//					'rating': 3,
//					'preference': {'music': true, 'blabla': false},
//					'response': false,
//				},
//				{
//					'firstname': 'Ayoub',
//					'lastname': 'Bahssou',
//					'avis': 6,
//					'age': 22,
//					'img': 'assets/img/hatim.png',
//					'rating': 3,
//					'img': 'assets/img/hatim.png',
//					'email': 'ayoubbahssou@gmail.com',
//					'preference': {'music': true, 'blabla': true},
//					'response': false,
//				},
				{
					'firstname': 'Ilyass',
					'lastname': 'Arrajoum',
					'email': 'arrajoumilyass@gmail.com',
					'avis': 6,
					'img': 'assets/img/ilyasse.jpg',
					'age': 23,
					'rating': 3,
					'preference': {'music': false, 'blabla': false},
					'response': false,
				},
			],
		};

		return api;
    }
})();
