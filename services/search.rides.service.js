/**
 * Created by ahadri on 1/31/17.
 */

(function() {
    angular.module('main')
        .service('SearchRidesService', SearchRidesService);

	/**
	 *
	 * @constructor
	 */
	function SearchRidesService() {
		let api = {
			'searchRides': [
				{
					'departure': 'Marrakech',
					'arrival': 'Casablanca',
					'date': new Date().toDateString(),
					'placesAvailable': 2,
					'price': 50,
					'details': 'Start from Marrakech, Jamaa El Fana At 17H',
					'driver': {
						'firstname': 'Hatim',
						'lastname': 'Ahadri',
						'avis': 6,
						'email': 'ahadrihatim@gmail.com',
						'age': 22,
						'img': 'assets/img/hatim.png',
						'rating': 3,
						'preference': {'music': true, 'blabla': false},
					},
					'car': {'mark': 'Peugot', 'color': 'White', 'matricule': 'b-12345'},
				},
//				},
//				{
//					'departure': 'Casablanca',
//					'arrival': 'Agadir',
//					'date': new Date().toDateString(),
//					'placesAvailable': 1,
//					'price': 50,
//					'driver': {
//						'firstname': 'Ayoub',
//						'lastname': 'Bahssou',
//						'avis': 6,
//						'age': 22,
//						'img': 'assets/img/ayoub.png',
//						'rating': 3,
//						'email': 'ayoubbahssou@gmail.com',
//						'preference': {'music': true, 'blabla': true},
//					},
//					'car': {'mark': 'Clio', 'color': 'Gris Clair', 'matricule': '12-b-102'},
//				},
//				{
//					'departure': 'Tanger',
//					'arrival': 'Rabat',
//					'date': new Date().toDateString(),
//					'placesAvailable': 3,
//					'price': 20,
//					'driver': {
//						'firstname': 'Ilyass',
//						'lastname': 'Arrajoum',
//						'email': 'arrajoumilyass@gmail.com',
//						'avis': 6,
//						'img': 'assets/img/ilyass.png',
//						'age': 25,
//						'rating': 3,
//						'preference': {'music': false, 'blabla': false},
//					},
//					'car': {'mark': 'Clio', 'color': 'Gris Clair', 'matricule': '12-b-102'},
//				},
//				{
//					'departure': 'Fes',
//					'arrival': 'Marrakech',
//					'date': new Date().toDateString(),
//					'placesAvailable': 2,
//					'price': 30,
//					'driver': {
//						'firstname': 'Mohamed',
//						'lastname': 'Hassan',
//						'email': 'hassanmohamed@gmail.com',
//						'avis': 6,
//						'img': 'assets/img/mohamed.png',
//						'age': 50,
//						'rating': 3,
//						'preference': {'music': true, 'blabla': true},
//					},
//					'car': {'mark': 'Clio', 'color': 'Gris Clair', 'matricule': '12-b-102'},
//				},
			],
		};
		return api;
	}
})();
